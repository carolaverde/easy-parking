package com.example.hardparkingv1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class Hardparkingv1Application {

    public static void main(String[] args) {
        SpringApplication.run(Hardparkingv1Application.class, args);
    }

    @RestController
    public class HelloController {

        @RequestMapping("/")
        public String hello() {
            return "Hola estoy funcionando de modo RestController: ";
        }
    }
}
