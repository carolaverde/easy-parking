/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.hardparkingv1.Controladores;

import com.example.hardparkingv1.Modelo.Bahia;
import com.example.hardparkingv1.Servicios.BahiaServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Atxel
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/bahia")
public class BahiaControlador {

    @Autowired
    private BahiaServicio bahiaServicio;

    @PostMapping(value = "/")
    public ResponseEntity<Bahia> agregar(@RequestBody Bahia bahia) {
        Bahia disp = bahiaServicio.save(bahia);
        return new ResponseEntity<>(disp, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Bahia> eliminar(@PathVariable Integer id) {
        Bahia disp = bahiaServicio.findById(id);
        if (disp != null) {
            bahiaServicio.delete(id);
        } else {
            return new ResponseEntity<>(disp, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(disp, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Bahia> editar(@RequestBody Bahia bahia) {
        Bahia disp = bahiaServicio.findById(bahia.getIdBahia());
        if (disp != null) {
            disp.setIdBahia(bahia.getIdBahia());
            disp.setDisponible(bahia.getDisponible());
            disp.setVehiculo(bahia.getVehiculo());
            disp.setUsuarioSistema(bahia.getUsuarioSistema());
            bahiaServicio.save(disp);
        } else {
            return new ResponseEntity<>(disp, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(disp, HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<Bahia> consultarTodo() {
        return bahiaServicio.findAll();
    }

    @GetMapping("/list/{id}")
    public Bahia consultaPorId(@PathVariable Integer id) {
        return bahiaServicio.findById(id);
    }
}
