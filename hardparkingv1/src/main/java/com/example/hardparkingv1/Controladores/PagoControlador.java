/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.hardparkingv1.Controladores;

import com.example.hardparkingv1.Modelo.Pago;
import com.example.hardparkingv1.Servicios.PagoServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Atxel
 */

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/pago")
public class PagoControlador {
    @Autowired    
private PagoServicio pagoServicio;
    
    @PostMapping(value="/")
    public ResponseEntity<Pago> agregar(@RequestBody Pago pago){
        Pago factura = pagoServicio.save(pago);
        return new ResponseEntity<>(factura,HttpStatus.OK);
    }
    
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Pago> eliminar(@PathVariable Integer id){
    Pago factura = pagoServicio.findById(id);
    if(factura!=null){
        pagoServicio.delete(id);
    }
    else{
        return new ResponseEntity<>(factura, HttpStatus.INTERNAL_SERVER_ERROR);
    }
        return new ResponseEntity<>(factura, HttpStatus.OK);
    }
      
    @PutMapping(value="list/{id}")
public ResponseEntity<Pago> editar(@RequestBody Pago pago){
Pago factura = pagoServicio.findById(pago.getIdpago());
if(factura!=null)
{
 factura.setHoraSalida(pago.getHoraSalida());
 factura.setBahia(pago.getBahia());
pagoServicio.save(factura);
}
else
 return new ResponseEntity<>(factura, HttpStatus.INTERNAL_SERVER_ERROR);
return new ResponseEntity<>(factura, HttpStatus.OK);
}
@GetMapping("/list")
public List<Pago> consultarTodo(){
return pagoServicio.findAll();
}
@GetMapping("/list/{id}")
public Pago consultaPorId(@PathVariable Integer id){
return pagoServicio.findById(id);
}
}
