/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.hardparkingv1.Controladores;

import com.example.hardparkingv1.Modelo.UsuarioSistema;
import com.example.hardparkingv1.Servicios.UsuarioSistemaServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Atxel
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/usuarioSistema")
public class UsuarioSistemaControlador {

    @Autowired
    private UsuarioSistemaServicio usuarioSistemaServicio;

    @PostMapping(value = "/")
    public ResponseEntity<UsuarioSistema> agregar(@RequestBody UsuarioSistema usuarioSistema) {
        UsuarioSistema usuario = usuarioSistemaServicio.save(usuarioSistema);
        return new ResponseEntity<>(usuario, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<UsuarioSistema> eliminar(@PathVariable Integer id) {
        UsuarioSistema usuario = usuarioSistemaServicio.findById(id);
        if (usuario != null) {
            usuarioSistemaServicio.delete(id);
        } else {
            return new ResponseEntity<>(usuario, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(usuario, HttpStatus.OK);
    }

    @PutMapping(value = "/list/{id}")
    public ResponseEntity<UsuarioSistema> editar(@RequestBody UsuarioSistema usuarioSistema) {
        UsuarioSistema usuario = usuarioSistemaServicio.findById(usuarioSistema.getIdusuarioSistema());
        if (usuario != null) {
            usuario.setNombre(usuarioSistema.getNombre());
            usuario.setApellido(usuarioSistema.getApellido());
            usuario.setDocIdentidad(usuarioSistema.getDocIdentidad());
            usuario.setTelefono(usuarioSistema.getTelefono());
            usuario.setCargo(usuarioSistema.getCargo());
            usuarioSistemaServicio.save(usuario);
        } else {
            return new ResponseEntity<>(usuario, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(usuario, HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<UsuarioSistema> consultarTodo() {
        return usuarioSistemaServicio.findAll();
    }

    @GetMapping("/list/{id}")
    public UsuarioSistema consultaPorId(@PathVariable Integer id) {
        return usuarioSistemaServicio.findById(id);
    }
}
