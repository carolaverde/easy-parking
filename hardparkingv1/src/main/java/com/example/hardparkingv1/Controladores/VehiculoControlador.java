/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.hardparkingv1.Controladores;

import com.example.hardparkingv1.Modelo.Vehiculo;
import com.example.hardparkingv1.Servicios.VehiculoServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author Atxel
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/vehiculo")
public class VehiculoControlador {

    @Autowired
    private VehiculoServicio vehiculoServicio;

    @GetMapping("/list")
    public List<Vehiculo> consultarTodo() {
        return vehiculoServicio.findAll();
    }

    @GetMapping("/list/{id}")
    public Vehiculo consultaPorId(@PathVariable Integer id) {
        return vehiculoServicio.findById(id);
    }

    @PostMapping(value = "/")
    public ResponseEntity<Vehiculo> agregar(@RequestBody Vehiculo vehiculo) {
        Vehiculo tipo = vehiculoServicio.save(vehiculo);
        return new ResponseEntity<>(tipo, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Vehiculo> eliminar(@PathVariable Integer id) {
        Vehiculo tipo = vehiculoServicio.findById(id);
        if (tipo != null) {
            vehiculoServicio.delete(id);
        } else {
            return new ResponseEntity<>(tipo, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(tipo, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Vehiculo> editar(@RequestBody Vehiculo vehiculo) {
        Vehiculo tipo = vehiculoServicio.findById(vehiculo.getIdvehiculo());
        if (tipo != null) {
            tipo.setIdvehiculo(vehiculo.getIdvehiculo());
            tipo.setPlaca(vehiculo.getPlaca());
            tipo.setTipoVehiculo(vehiculo.getTipoVehiculo());
            tipo.setHoraIngreso(vehiculo.getHoraIngreso());
            tipo.setTarifa(vehiculo.getTarifa());
            vehiculoServicio.save(tipo);
        } else {
            return new ResponseEntity<>(tipo, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(tipo, HttpStatus.OK);
    }
}
