/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.hardparkingv1.Modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
/**
 *
 * @author Atxel
 */
@Entity
@Table(name="usuarioSistema")
public class UsuarioSistema implements Serializable{
// Atributos
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name="idusuarioSistema")
    private Integer idusuarioSistema;
@Column(name="nombre")
    private String nombre;
@Column(name="apellido")
    private String apellido;
@Column(name="docIdentidad")
    private String docIdentidad;
@Column(name="telefono")
    private String telefono;
@Column(name="cargo")
    private String cargo;

// Constructor
    public UsuarioSistema() {
    }

    public UsuarioSistema(Integer idusuarioSistema, String nombre, String apellido, String docIdentidad, String telefono, String cargo) {
        this.idusuarioSistema = idusuarioSistema;
        this.nombre = nombre;
        this.apellido = apellido;
        this.docIdentidad = docIdentidad;
        this.telefono = telefono;
        this.cargo = cargo;
    }
        
// Métodos get & set

    public Integer getIdusuarioSistema() {
        return idusuarioSistema;
    }

    public void setIdusuarioSistema(Integer idusuarioSistema) {
        this.idusuarioSistema = idusuarioSistema;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDocIdentidad() {
        return docIdentidad;
    }

    public void setDocIdentidad(String docIdentidad) {
        this.docIdentidad = docIdentidad;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
    
// Métodos adicionales

}
