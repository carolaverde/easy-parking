/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.hardparkingv1.Modelo;

import java.time.LocalDateTime;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Atxel
 */
@Entity
@Table(name="pago")
public class Pago implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name ="idpago")
    private Integer idpago;
    
    @Column (name="idbahia")
    private Bahia bahia;
    
    @Column(name="horaSalida")
    private String horaSalida;
    

    public Pago() {
    }

    public Pago(Integer idpago, Bahia bahia, String horaSalida) {
        this.idpago = idpago;
        this.bahia = bahia;
        this.horaSalida = horaSalida;
    }

    public Integer getIdpago() {
        return idpago;
    }

    public void setIdpago(Integer idpago) {
        this.idpago = idpago;
    }

    public Bahia getBahia() {
        return bahia;
    }

    public void setBahia(Bahia bahia) {
        this.bahia = bahia;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

}