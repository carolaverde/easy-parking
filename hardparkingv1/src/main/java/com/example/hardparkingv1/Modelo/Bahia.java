/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.hardparkingv1.Modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Atxel
 */
@Entity
@Table(name="bahia")
public class Bahia implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer idBahia;
    @Column(name="disponible")
    private String disponible;
    @Column (name="idvehiculo")
    private String vehiculo;
    @Column (name="idusuarioSistema")
    private String usuarioSistema;

    public Bahia() {
    }

    public Bahia(Integer idBahia, String disponible, String vehiculo, String usuarioSistema) {
        this.idBahia = idBahia;
        this.disponible = disponible;
        this.vehiculo = vehiculo;
        this.usuarioSistema = usuarioSistema;
    }

    public Integer getIdBahia() {
        return idBahia;
    }

    public void setIdBahia(Integer idBahia) {
        this.idBahia = idBahia;
    }

    public String getDisponible() {
        return disponible;
    }

    public void setDisponible(String disponible) {
        this.disponible = disponible;
    }

    public String getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(String vehiculo) {
        this.vehiculo = vehiculo;
    }

    public String getUsuarioSistema() {
        return usuarioSistema;
    }

    public void setUsuarioSistema(String usuarioSistema) {
        this.usuarioSistema = usuarioSistema;
    }

    
}
