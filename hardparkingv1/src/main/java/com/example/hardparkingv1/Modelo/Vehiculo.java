/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.hardparkingv1.Modelo;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.GenericGenerator;
import java.time.LocalDateTime;
/**
 *
 * @author Jair Jimenez
 */

@Entity
@Table(name="vehiculo")
public class Vehiculo implements Serializable{
     
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer idvehiculo;
    
    @Column (name="placa")
    private String placa;
    
    @Column (name="tipoVehiculo")
    private String tipoVehiculo;
    
    @Column (name="horaIngreso")
    private String horaIngreso;
    
    @Column (name="tarifa")
    private Integer tarifa;

    public Vehiculo() {
    }

    public Vehiculo(Integer idvehiculo, String placa, String tipoVehiculo, String horaIngreso, Integer tarifa) {
        this.idvehiculo = idvehiculo;
        this.placa = placa;
        this.tipoVehiculo = tipoVehiculo;
        this.horaIngreso = horaIngreso;
        this.tarifa = tarifa;
    }

    public Integer getIdvehiculo() {
        return idvehiculo;
    }

    public void setIdvehiculo(Integer idvehiculo) {
        this.idvehiculo = idvehiculo;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(String tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    public String getHoraIngreso() {
        return horaIngreso;
    }

    public void setHoraIngreso(String horaIngreso) {
        this.horaIngreso = horaIngreso;
    }

    public Integer getTarifa() {
        return tarifa;
    }

    public void setTarifa(Integer tarifa) {
        this.tarifa = tarifa;
    }

    
}