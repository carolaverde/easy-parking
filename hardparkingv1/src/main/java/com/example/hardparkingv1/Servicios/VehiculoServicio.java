/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.hardparkingv1.Servicios;


import com.example.hardparkingv1.Modelo.Vehiculo;
import java.util.List;

/**
 *
 * @author Jair Jimenez
 */
public interface VehiculoServicio {
    
    public Vehiculo save (Vehiculo vehiculo);
    
    public void delete (Integer id);
    
    public Vehiculo findById (Integer id);
    
    public List<Vehiculo> findAll();
    
    
}
