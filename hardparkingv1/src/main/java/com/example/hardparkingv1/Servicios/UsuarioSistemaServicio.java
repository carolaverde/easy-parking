/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.hardparkingv1.Servicios;

import com.example.hardparkingv1.Modelo.UsuarioSistema;
import java.util.List;

/**
 *
 * @author Atxel
 */
public interface UsuarioSistemaServicio {
    public UsuarioSistema save(UsuarioSistema usuarioSistema);
    public void delete(Integer id);
    public UsuarioSistema findById(Integer id);
    public List<UsuarioSistema> findAll();
}
