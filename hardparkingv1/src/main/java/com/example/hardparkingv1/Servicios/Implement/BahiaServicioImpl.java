/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.hardparkingv1.Servicios.Implement;

import com.example.hardparkingv1.Dao.BahiaDao;
import com.example.hardparkingv1.Modelo.Bahia;
import com.example.hardparkingv1.Servicios.BahiaServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Carolina
 */
@Service
public class BahiaServicioImpl implements BahiaServicio {

    @Autowired
    private BahiaDao bahiaDao;

    @Override
    @Transactional(readOnly = false)
    public Bahia save(Bahia bahia) {
        return bahiaDao.save(bahia);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        bahiaDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Bahia findById(Integer id) {
        return bahiaDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Bahia> findAll() {
        return (List<Bahia>) bahiaDao.findAll();
    }

}
