/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.hardparkingv1.Servicios.Implement;

import com.example.hardparkingv1.Dao.PagoDao;
import com.example.hardparkingv1.Modelo.Pago;
import com.example.hardparkingv1.Servicios.PagoServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author BE77OCAS
 */

@Service
public class PagoServicioImpl implements PagoServicio{
    
    @Autowired
    private PagoDao pagoDao;
    
    @Override
    @Transactional (readOnly=false)
          
    public Pago save(Pago pago) {
    return pagoDao.save(pago);
    }

    @Override
    @Transactional (readOnly = false)
    public void delete(Integer id) {
    pagoDao.deleteById(id);
    }

    @Override
    @Transactional (readOnly = true)
    public Pago findById(Integer id) {
    return pagoDao.findById(id).orElse(null);
    }

    @Override
    @Transactional (readOnly = true)
    public List<Pago> findAll() {
    return (List<Pago>) pagoDao.findAll();
   }
    
}
