/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.hardparkingv1.Servicios;

import com.example.hardparkingv1.Modelo.Bahia;
import java.util.List;

/**
 *
 * @author Carolina
 */
public interface BahiaServicio {
    public Bahia save(Bahia bahia);
    public void delete (Integer id);
    public Bahia findById (Integer id);
    public List<Bahia> findAll();
}
