/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.hardparkingv1.Servicios;

import com.example.hardparkingv1.Modelo.Pago;
import java.util.List;

/**
 *
 * @author BE77OCAS
 */
public interface PagoServicio {
    
      //Metodo
    public Pago save (Pago pago);
    public void delete (Integer id);
    public Pago findById (Integer id);
    public List<Pago> findAll();
    
}
