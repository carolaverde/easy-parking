/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.hardparkingv1.Servicios.Implement;

import com.example.hardparkingv1.Modelo.UsuarioSistema;
import com.example.hardparkingv1.Dao.UsuarioSistemaDao;
import com.example.hardparkingv1.Servicios.UsuarioSistemaServicio;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Atxel
 */
@Service
public class UsuarioSistemaServicioImpl implements UsuarioSistemaServicio {

@Autowired
private UsuarioSistemaDao usuarioSistemaDao;

@Override
@Transactional(readOnly=false)

public UsuarioSistema save(UsuarioSistema usuarioSistema){
    return usuarioSistemaDao.save(usuarioSistema);
}

    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id) {
    usuarioSistemaDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly=true)

    public UsuarioSistema findById(Integer id) {
    return usuarioSistemaDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly=true)

    public List<UsuarioSistema> findAll() {
    return (List<UsuarioSistema>) usuarioSistemaDao.findAll(); 
            }
 
}
