/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.hardparkingv1.Dao;

import com.example.hardparkingv1.Modelo.UsuarioSistema;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Atxel
 */
public interface UsuarioSistemaDao extends CrudRepository<UsuarioSistema, Integer>{
    
}
