package com.example.hardparkingv1.Dao;


import com.example.hardparkingv1.Modelo.Pago;
import org.springframework.data.repository.CrudRepository;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */

/**
 *
 * @author BE77OCAS
 */
public interface PagoDao extends CrudRepository<Pago,Integer>{
    
}
