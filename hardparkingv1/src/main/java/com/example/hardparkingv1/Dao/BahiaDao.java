/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.hardparkingv1.Dao;

import com.example.hardparkingv1.Modelo.Bahia;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Carolina
 */
public interface BahiaDao extends CrudRepository<Bahia, Integer> {
    
}
