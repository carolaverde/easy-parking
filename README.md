# Proyecto EASY PARKING

## Índice

* [1. Definición del problema](#1-Definición del problema)
* [2. Resumen del proyecto](#2-resumen-del-proyecto)



***

## 1. Definición del problema

El administrador del parqueadero Easy Parking, el cual cuenta con plazas disponibles para 50 vehículos tipo automóvil y/o camioneta y 30 plazas disponibles para moto, desea automatizar la operación del negocio, de tal forma que se pueda llevar a cabo el control de facturación y la trazabilidad diaria de ingresos de vehículos.



## 2. Resumen del proyecto
 
 Este proyecto se realizó para el Proyecto Mintic 2022; es un trabajo colaborativo apoyándonos en la plataforma GitLab y aplicando el conocimiento en la terminal con comandos Git. La base de datos se trabajo en MySQL Workbench y creando diagrama para poder identificar los atributos a relacionar con nuestro proyecto final, al igual que las Primary Key y las foreign Key. La elaboración del proyecto se realizo en Java con la ayuda del framework SpringBoot; para finalizar el FrontEnd se desarrollo en HTML, Bootstrap y Javascript para las funcionalidades y la conexión con la base de datos. 



