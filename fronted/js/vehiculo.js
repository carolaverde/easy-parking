

function loadDataconsulta() {
    let request = sendRequest('vehiculo/list/', 'GET', '')
    let table = document.getElementById('cars-table');
    table.innerHTML = "";
    request.onload = function () {

        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr>
                    <th>${element.idvehiculo}</th>
                    <td>${element.placa}</td>
                    <td>${element.tipoVehiculo}</td>
                    <td>${element.horaIngreso}</td>
                    <td>${element.tarifa}</td>
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "/form_vehiculo.html?id=${element.idvehiculo}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deletevehiculo(${element.idvehiculo})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function () {
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos.</td>
            </tr>
        `;
    }
}

function editarVehiculo(idvehiculo) {
    let request = sendRequest('vehiculo/list/' + idvehiculo, 'GET', '')
    let placados = document.getElementById('placa')
    let tipoVehiculo = document.getElementById('tipoVehiculo')
    let horaIngreso = document.getElementById('horaIngreso')
    let tarifa = document.getElementById('tarifa')
    let id = document.getElementById('id')
    request.onload = function () {

        let data = request.response
        id.value = data.idvehiculo
        placados.value = data.placa
        tipoVehiculo.value = data.tipoVehiculo
        horaIngreso.value = data.horaIngreso
        tarifa.value = data.tarifa
    }
    request.onerror = function () {
        alert("Error al recuperar los datos.");
    }
}

function deletevehiculo(idvehiculo) {
    let request = sendRequest('vehiculo/' + idvehiculo, 'DELETE', '')
    request.onload = function () {
        loadDataconsulta()
    }
}

function savevehiculoGuardar() {
    let placados = document.getElementById('placa').value
    let tipoVehiculo = document.getElementById('tipoVehiculo').value
    let horaIngreso = document.getElementById('horaIngreso').value
    let tarifa = document.getElementById('tarifa').value
    let id = document.getElementById('id').value
    let data = { 'idvehiculo': id, 'placa': placados, 'tipoVehiculo': tipoVehiculo, 'horaIngreso': horaIngreso, 'tarifa': tarifa }
    let request = sendRequest('vehiculo/', id ? 'PUT' : 'POST', data)
    request.onload = function () {
        window.location = 'vehiculo.html';
    }
    request.onerror = function () {
        alert('Error al guardar los cambios.')
    }
}