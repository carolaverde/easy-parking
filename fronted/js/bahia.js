

function loadDataconsulta() {
    let request = sendRequest('bahia/list/', 'GET', '')
    let table = document.getElementById('bahia-table');
    table.innerHTML = "";
    request.onload = function () {

        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr>
                    <th>${element.idBahia}</th>
                    <td>${element.disponible}</td>
                    <td>${element.vehiculo}</td>
                    <td>${element.usuarioSistema}</td>
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "/form_bahia.html?id=${element.idBahia}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deletebahia(${element.idBahia})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function () {
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos.</td>
            </tr>
        `;
    }
}

function editarbahia(idBahia) {
    let request = sendRequest('bahia/list/' + idBahia, 'GET', '')
    let estado = document.getElementById('bahia-estado')
    let tipo = document.getElementById('bahia-tipo')
    let usuario = document.getElementById('bahia-usuario')
    let id = document.getElementById('id')
    request.onload = function () {

        let data = request.response
        id.value = data.idBahia
        estado.value = data.disponible
        tipo.value = data.vehiculo
        usuario.value = data.usuarioSistema
    }
    request.onerror = function () {
        alert("Error al recuperar los datos.");
    }
}

function deletebahia(idBahia) {
    let request = sendRequest('bahia/' + idBahia, 'DELETE', '')
    request.onload = function () {
        loadDataconsulta()
    }
}

function savebahiaGuardar() {
    let estado = document.getElementById('bahia-estado').value
    let tipo = document.getElementById('bahia-tipo').value
    let usuario = document.getElementById('bahia-usuario').value
    let id = document.getElementById('id').value
    let data = { 'idBahia': id, 'disponible': estado, 'vehiculo': tipo, 'usuarioSistema': usuario }
    let request = sendRequest('bahia/', id ? 'PUT' : 'POST', data)
    request.onload = function () {
        window.location = 'bahia.html';
    }
    request.onerror = function () {
        alert('Error al guardar los cambios.')
    }
}